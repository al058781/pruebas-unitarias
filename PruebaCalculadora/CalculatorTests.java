package Calculator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Henrr
 */
public class CalculatorTests {

    operaciones calculator1 = new operaciones();

    public CalculatorTests() {
    }

    @Test
    public void testSuma1() {
        assertEquals(5, calculator1.suma(2, 3));
    }

    @Test
    public void testSuma2() {
        assertEquals(6, calculator1.suma(3, 3));
    }

    @Test
    public void testResta1() {
        assertEquals(2, calculator1.resta(5, 3));
    }

    @Test
    public void testResta2() {
        assertEquals(3, calculator1.resta(6, 3));
    }

    @Test
    public void testMultipliacion1() {
        assertEquals(40, calculator1.multiplicacion(10, 4));
    }

    @Test
    public void testMultipliacion2() {
        assertEquals(30, calculator1.multiplicacion(6, 5));
    }

    @Test
    public void testDivision1() {
        assertEquals(2, calculator1.division(4, 2));
    }

    @Test
    public void testDivision2() {
        assertEquals(10, calculator1.division(30, 3));
    }
    
    @Test
    public void testExponenciacion1() {
        assertEquals(16, calculator1.exponenciacion(2, 4));
    }

    @Test
    public void testExponenciacion2() {
        assertEquals(64, calculator1.exponenciacion(4, 3));
    }

}
