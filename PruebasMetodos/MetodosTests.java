package Metodos;

import MetodosParaProbar.Metodos;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;


/**
 *
 * @author Henrr
 */
public class MetodosTests {
    Metodos Metodo = new Metodos();
    
    public MetodosTests() {
    }
    @Test
    public void testLessThanOrEqualToZero1() {
        assertEquals(true, Metodo.lessThanOrEqualToZero(0));
    }
    @Test
    public void testLessThanOrEqualToZero2() {
        assertEquals(false, Metodo.lessThanOrEqualToZero(1));
    }
    @Test
    public void testLessThanOrEqualToZero3() {
        assertEquals(true, Metodo.lessThanOrEqualToZero(-1));
    }
    @Test
    public void testStutter1() {
        assertEquals("Ex... Ex... Excelente?", Metodo.stutter("Excelente"));
    }
     @Test
    public void testStutter2() {
        assertEquals("Al... Al... Alegre?", Metodo.stutter("Alegre"));
    }
     @Test
    public void testStutter3() {
        assertEquals("Em... Em... Emocionante?", Metodo.stutter("Emocionante"));
    }
    @Test
    public void testTotalCups1() {
        assertEquals(23, Metodo.totalCups(20));
    }
    @Test
    public void testTotalCups2() {
        assertEquals(42, Metodo.totalCups(36));
    }
    @Test
    public void testTotalCups3() {
        assertEquals(298, Metodo.totalCups(256));
    }
     @Test
    public void testCountWords1() {
        assertEquals(2, Metodo.countWords("Buenos días"));
    }
    @Test
    public void testCountWords2() {
        assertEquals(6, Metodo.countWords("Hola Mundo. Esto es una prueba"));
    }
    @Test
    public void testCountWords3() {
        assertEquals(3, Metodo.countWords("¿Quién es usted?"));
    }
}
