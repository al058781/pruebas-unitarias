package MetodosParaProbar;

import java.util.StringTokenizer;

/**
 *
 * @author Henrr
 */
public class Metodos {
    public boolean lessThanOrEqualToZero(int num) {
        if (num < 0) {
            return true;
        } else if (num == 0) {
            return true;
        } else {
            return false;
        }
    }
    public String stutter(String pal){
        String frase,dosLetras;
        dosLetras= pal.charAt(0)+""+pal.charAt(1)+"... ";
        frase=dosLetras+dosLetras+pal+"?";
    return frase;
    }
    
    public int totalCups(int tazas){
        int tazasGratis= (int)tazas / 6 ;
        
        return tazasGratis+tazas;
    }
   public int countWords(String Frase){
       StringTokenizer nPalabras= new StringTokenizer(Frase);
       
       return  nPalabras.countTokens();
   } 
}
